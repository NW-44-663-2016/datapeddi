using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataPranayPeddi.Models;

namespace DataPranayPeddi.Controllers
{
    public class DirectorsController : Controller
    {
        private ApplicationDbContext _context;

        public DirectorsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Directors
        public IActionResult Index()
        {
            var applicationDbContext = _context.Director.Include(d => d.Location);
            return View(applicationDbContext.ToList());
        }

        // GET: Directors/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Director director = _context.Director.Single(m => m.DirectorID == id);
            if (director == null)
            {
                return HttpNotFound();
            }

            return View(director);
        }

        // GET: Directors/Create
        public IActionResult Create()
        {
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: Directors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Director director)
        {
            if (ModelState.IsValid)
            {
                _context.Director.Add(director);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", director.LocationID);
            return View(director);
        }

        // GET: Directors/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Director director = _context.Director.Single(m => m.DirectorID == id);
            if (director == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", director.LocationID);
            return View(director);
        }

        // POST: Directors/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Director director)
        {
            if (ModelState.IsValid)
            {
                _context.Update(director);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", director.LocationID);
            return View(director);
        }

        // GET: Directors/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Director director = _context.Director.Single(m => m.DirectorID == id);
            if (director == null)
            {
                return HttpNotFound();
            }

            return View(director);
        }

        // POST: Directors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Director director = _context.Director.Single(m => m.DirectorID == id);
            _context.Director.Remove(director);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
